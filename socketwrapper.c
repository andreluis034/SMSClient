#include "socketwrapper.h"

/**
 * Inicializar o socket no ip:porta dados 
 * 
 * @param   {char*} IP a que o servidor vai ser criado
 * @param   {int} Porta em que ira inicializar o servidor
 * @return  {int} Socket Criado
 */
int initSocket(char* ip, int port)
{
    struct sockaddr_in dest;

	/* Chamar socket()
	**	AF_INET - support IPv4
	**	SOCK_STREAM - Enviar e receber dados
	*/
    socketf = socket(AF_INET, SOCK_STREAM, 0);
    if ( socket < 0 )
    {
        perror("Error ao abrir socket");
        return -1;
    }

    //Inicializar a struct stockaddr_in(address/port)
    memset(&dest, 0, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(port);
    if ( inet_aton(ip, &dest.sin_addr) == 0 )
    {
        perror(ip);
        return -1;
    }

    //Conectar ao servidor
    if ( connect(socketf, (struct sockaddr*)&dest, sizeof(dest)) != 0 )
    {
        perror("Error ao conectar");
        return -1;
    }

	return 0; //Criamos o socket com sucesso
}

/**
 * Le os conteudos no socket para um buffer dado(Pode ser realocado)
 * 
 * @param   {void**}    Apontador para o apontador de o buffer, este pode ser realocado caso não 
 *                          seja grande suficiente
 * @param   {int}       Tamanho inicial do buffer
 * @return  {int}       Tamanho lido
 */
int readSocket(void** buffer, int length)
{
    Message* m = *((Message **)buffer);
    memset(*buffer, 0, length);
    int status = read(socketf, *buffer, 4); //Lemos os quatro primeiros bytes que são o Message.Length

    if (status >= 0)
    {
        if (m->Length > length)
        {
           length = m->Length;
           *buffer = realloc(*buffer, length);
        }
        //Queremos ler a informacao a asseguir à Length por isso o nosso endereço onde vamos
        //escrever passa a ser buffer + sizeof(int)
        status = read(socketf, (void*)(*buffer + sizeof(int)), length-sizeof(int));
    }

	if (status < 0 && errno != EWOULDBLOCK && errno != EBADF)
	{
		perror("Erro ao ler socket");
	}
	return status;
}
/**
 * Escreve o buffer dado no socket
 * 
 * @param   {void*}     Buffer que vai se escrito  no socket
 * @param   {int}       Tamanho do buffer
 * @return
 */
int writeSocket(void* buffer, int size)
{
	int status = write(socketf, buffer, size);
	if (status < 0)
	{
		perror("Error ao escrever no socket");
	}
	return status;
}
/**
 * Faz com que o read() não bloque a execução do programa
 */
void setNonBlocking()
{
    int flags = fcntl(socketf, F_GETFL, 0);
    fcntl(socketf, F_SETFL, flags | O_NONBLOCK);
}

void closeSocket()
{
	close(socketf);
}
