#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <unistd.h> // used for sleep()
#include <errno.h>
#include "socketwrapper.h"
#include "messagehelper.h"
#include "defs.h"
#include <fcntl.h>
#include "ctype.h"


//#include <netdb.h>
//#include <netinet/in.h>
int exitClient = 0;
int opcao = 0;
int requestedUsers = 0;
int writingMessage = 0;
void PrintMenu();
/**
 * Fazer Login no servidor dador
 * @param  {char*} 		Utilizador dado no argumento quando o programa é executado
 * @param  {char*} 		Ip do serivodr a conector
 * @param  {int} 		Porta do servidor
 * @return {int}		Estado do login
 *                		0 - Login falhado
 *                		1 - Login aceite
 */
int Login(char* username, char* host, int port)//Restringimos username a tamanho 20
{
	Message* rcvMessage = malloc(1000);
	Message* sndMessage;
	
	char userpassCombo[MAX_USER_LENGTH*2];

	int i = 0;
	for (i = 0; username[i] != 0; ++i)
	{
		username[i] = tolower(username[i]);
	}
	for (i = 0; i < 3; ++i)
	{
		char* password;
		password = getpass("Password? ");
		sprintf(userpassCombo, "%s %s", username, password);
		sndMessage = createMessage(LOGIN1, "\0", userpassCombo);
		initSocket(host, port);
		if(writeSocket(sndMessage, sndMessage->Length) < 0)
			return 0;
		memset(rcvMessage, 0, 1000);
		if(readSocket((void**)&rcvMessage, 1000) < 0)
			return 0;	

		free(sndMessage);
		if (rcvMessage->Type == OK)
		{
			free(rcvMessage);
			return 1;
		}	
		printf("%sA combinação utilizador/password dada não existe %s\n",KRED, KNRM);
		closeSocket();
	}
	free(rcvMessage);
	return 0;
}

/**
 * Thread para receber as mensagens do servidor
 */
void *receiveThread()
{
	int status;
	setNonBlocking();
	Message* rcvMessage = malloc(1000); 

	while(1)
	{
		if (exitClient)
			break;
		if(writingMessage == 1)
		{
			usleep(100000);
			continue;	
		}
		//char buffer[1000];
		status = readSocket((void**)&rcvMessage, 1000);
		if ( status < 0)
		{
			switch(errno)
			{
				case EWOULDBLOCK:
						//Para não consumir recursos desnecessariamente;
						//100000 microsegundos = 0.1 segundos
						usleep(100000);
						continue;
					break;
				case EBADF:
					free(rcvMessage);

					return NULL;
					break;
			}
		}
		/*
		** Como message é do tipo Message*(apontador)
		** temos de usar -> para aceder ao membro da sruct
		** ou alternativamente (*message).membro
		*/
		switch(rcvMessage->Type)
		{
			case RECVMSG:
				//clear(); //Indeciso em deixar isto
				printf("%s disse: %s\n", rcvMessage->Sender, rcvMessage->Content);
				switch(opcao)
				{
					case 0:
						//PrintMenu();
						break;
					default:
						break;
				}
				break;
			case LOGOUT:
				printf("%sLogout forçado: %s\n%s", 
					KRED, rcvMessage->Content, KNRM);
				exitClient = 1;
				break;
			case HALT:
				puts("!!!!! Servidor informou que irá terminar o serviço de SMS !!!!!");
				printf("%sLogout forçado: %s\n%s", 
					KRED, rcvMessage->Content, KNRM);
				exitClient = 1;
				break;

			case INFO:
				if(requestedUsers==1)
				{
					printf("Utilizadores online\n");
					requestedUsers = 0;
				}
				printf("%s\n",rcvMessage->Content);
				break;
		}
	}
	free(rcvMessage);
	//printf("Fechar socket\n");
	closeSocket();
	exit(0);
	return NULL;
}
/**
 * Envia o comando ao servidor para recebermos a lista de utilizadores online
 */
void onlineUsers()
{

	Message* getOnlineusers;

	getOnlineusers = createMessage(INFO, "server", "/onlineusers");

	writeSocket(getOnlineusers, getOnlineusers->Length);

	free(getOnlineusers);


}

//Imprime o menu
void PrintMenu()
{
	puts("\n**Menu**");
	puts("1) Listar utilizadores online");
	puts("2) Mandar SMS a um utilizador");
	puts("3) Limpar o ecra");
	puts("4) Logout\n");
}

/**
 * Le a opcao selecionada pelo utilizador
 * @return {int}		Opcao selecionada
 */
int Menu()
{
	opcao = 0;
	PrintMenu();
	scanf("%d", &opcao);getchar(); //Consumir \n
	if (exitClient)
	{
		return -1;
	}
	return (opcao > 0 && opcao < 5 ? opcao : Menu());
}

/**
 * Loop para enviar mensagens
 */
void mainLoop()
{
	int status;
	Message* sndMessage;// = malloc(1000);
	while(1)
	{
		switch(Menu())
		{
			case 1:
				requestedUsers = 1;
				onlineUsers();
				break;
			case 2:
				//Como o conteudo do socket está a ser lido num thread separado temos de controlar o stdout
				writingMessage = 1;
				sendMessage();
				writingMessage = 0;
				break;
			case 3:
				clear();
				initialPos();
				break;
			case 4:
				//anunciar said ao servidor
				sndMessage = createMessage(LOGOUT, "\0", "\0");

				writeSocket(sndMessage, sndMessage->Length);
				exitClient = 1;
				free(sndMessage);
				break;
			default:
				break;
		}
		//clear();
		if (exitClient)
			break;
	}
}

int main(int argc, char *argv[])
{
	if (argc < 2) // Utilizção errada
	{
      	fprintf(stderr,"Utilização: %s username [host] [port]\n", argv[0]);
      	exit(0);
	}
	char *host = argc > 2 ? argv[2] : "127.0.0.1"; 	//Default ip
	int port = argc > 3 ? atoi(argv[3]) : 1337;		//Default port
	printf("A conectar a: %s:%d\n", host, port);

	if (Login(argv[1], host, port) == 0)
		return 0;

	//Thread para receber mensagens a qualquer altura
	pthread_t tid; //ID do thread
	pthread_create(&tid, NULL, &receiveThread, (NULL));	
	clear();
	initialPos();	
	printf(KGRN "Utilizador %s autenticado. Pode começar a usar o sistema!\n" KNRM, 
		argv[1]);

	mainLoop();
	//Esperar que o thread adicional pare
	pthread_join(tid,NULL);

	return 0;
}
