#!/bin/sh

if [ "$1" = "debug" ]; then
	gcc -o SMSClient main.c messagehelper.c socketwrapper.c -lpthread -D __DEBUG__ -O0 -g
else
	gcc -o SMSClient main.c messagehelper.c socketwrapper.c -lpthread
fi

