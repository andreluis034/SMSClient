//http://stackoverflow.com/questions/3585846/color-text-in-terminal-aplications-in-unix
//https://en.wikipedia.org/wiki/ANSI_escape_code
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[1;31m"
#define KGRN  "\x1B[1;32m"
#define KYEL  "\x1B[1;33m"
#define KBLU  "\x1B[1;34m"
#define KMAG  "\x1B[1;35m"
#define KCYN  "\x1B[1;36m"
#define KWHT  "\x1B[1;37m"
//http://www.termsys.demon.co.uk/vtansi.htm
#define clear() printf("\x1B[2J");
#define eraseUp() printf("\x1B[1J");
#define initialPos() printf("\x1B[H");

#define MAX_USERS 40
#define MAX_ONLINE 40
#define MAX_USER_LENGTH 40