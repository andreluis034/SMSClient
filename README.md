André Brandão		up201503320@fc.up.pt 	201503320   
Rúben Carvalho		up201508742@fc.up.pt   	201508742
  

# SMSClient

  

Simples cliente de envio e receção de mensagens via Internet.
Pode ser utilizado com o seguinte servidor: https://gitlab.com/andreluis034/SMSSeverLabCC

 - Envia e recebe mensagens de tamanho arbritário(desde que tenha memórias suficiente)    
   

### Compilação
Para compilar o servidor deve executar um dos seguintes comandos:
##### Release
   
```sh
gcc -o SMSClient main.c messagehelper.c socketwrapper.c -lpthread
```   
    
    
##### Debug
```sh
gcc -o SMSClient main.c messagehelper.c socketwrapper.c -lpthread -g -O0 -D __DEBUG__	
```
  
    
     
### Utilização

	▌ Executar o cliente:
		
		Todos os utilizadores que pretenderem comunicar entre si, devem entrar na pasta SMSClient e executar o seguinte comando:

		./SMSClient <username> [host] [port]

		Por exemplo:

		./SMSClient Manuel

		Se pretender conectar-se a outro servidor para além do 127.0.0.1:1337 deverá especificar no comando acima o seu [host] e [port].
		Por exemplo:

		./SMSClient Manuel 91.2.1.4 1338
		
		De seguida será pedida a password respetiva ao seu utilizador.
		Note que a password nao será escrita no stdout.
