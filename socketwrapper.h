#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include "messagehelper.h"
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>


int socketf;

int initSocket(char*, int);
int readSocket(void**, int);
//int writeSocket(char*,int); //Nao existe/funciona em C
int writeSocket(void*, int);
void setNonBlocking();
void closeSocket();
