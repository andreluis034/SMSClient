#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "defs.h"
#include <fcntl.h>
#include <unistd.h>
#include "socketwrapper.h"	

/*
** 	LOGIN1 	- Enviar username(Apensa enviada pelos clientes)
**	LOGIN2  - Envio e validacao do password
**	OK 		- Autenticado(Apenas enviado pelo servidor)
**	NOTOK	- Não Autenticado(Apenas enviado pelo servidor)
**	RECVMSG - Mensagem recebida(Apensa enviada pelo servidor)
**	SENDMSG - Mensagem recebida(Apensa enviada pelos clientes)
**	WARN	- Caso nao seja possivel enviar a mensagem
**	HALT	- Desligar servidor (Apenas enviado pelo servidor)
*/
typedef enum 
{
	LOGIN1, LOGIN2, LOGOUT, OK, NOTOK, RECVMSG, SENDMSG, WARN, HALT, INFO
} MessageType;

typedef struct
{
										//Posição na memória
	int Length;							//0x00
	MessageType Type; 					//0x04
	char Sender[MAX_USER_LENGTH];		//0x08
	char Destination[MAX_USER_LENGTH];	//0x08 + MAX_USER_LENGTH(default 20)
	//Na verdade o tamanho vai ser maior que por causa do "hack" com o malloc
	char Content[1];	   				//0x08 + MAX_USER_LENGTH*2
} Message;

int actualMessageSize(Message*);
void byteArrayToMessage(Message*, char*, int);
void messageToByteArray(char*, Message*, int);
Message* createMessage(MessageType, char*, char*);
void sendMessage();
void printMessage(Message*);
