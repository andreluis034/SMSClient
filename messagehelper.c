#include "messagehelper.h"

/**
 * Dado um apontador para uma mensagem(Tipo Message) retorna o seu tamanho
 * Isto é necessário pois as mensagens são alocadas dinamicamente
 * @param  {Message*}		Apontador para a mensagem
 * @return {int} 			Tamanho da mensagem
 */
int actualMessageSize(Message* message)
{
	//+1 é do null Byte no final da string
	//MAX_USER_LENGTH * 2 é o tamano do sender + user
	return sizeof(int) + sizeof(message->Type) + MAX_USER_LENGTH * 2 + strlen(message->Content) + 1;
}

/**
 * Cria uma mensagem a partir dos argumentos dados
 * @param  {MessageType}        Tipo da mensagem(definido em messagehelper.h)
 * @param  {Char*} 				Destino da mensagem
 * @param  {Char*}     			Mensagem
 * @return {Message*} 			Apontador para a mensagem
 */
Message* createMessage(MessageType mType, char* destination, char* byteArray)
{
	int size = sizeof(int) + sizeof(mType) + MAX_USER_LENGTH * 2 + strlen(byteArray)+1;
	Message* message = malloc(size); //malloc aloca no heap da aplicacao
	memset(message, 0, size);
	memcpy(&message->Length, &size, sizeof(int));
	memcpy(&message->Type, &mType, sizeof(mType));
	strcpy(message->Destination, destination);
	strcpy(message->Content, byteArray);

	return message;
}


/**
 * Pede o destino e o conteudo da mensagem, cria a mensagem e envia ao servidor
 */
void sendMessage()
{
	char* destino = NULL;
	char* line = NULL;
	ssize_t read = -1;
	size_t length = 0;
	Message* sndMessage;

	printf("Destino: "); 
	read = getline(&destino, &length, stdin);
	destino[read-1] = 0;

	printf("Mensagem: ");
	read = getline(&line, &length, stdin);
	line[read-1] = 0;

	sndMessage = createMessage(SENDMSG, destino, line);
	 writeSocket(sndMessage, sndMessage->Length);
	free(destino);
	free(line);
	free(sndMessage);
}

/**
 * Imprive a mensagem no stdout, principalmente para DEBUG
 * @param {Message*} 			Mensagem a imprimir no stdout
 */
void printMessage(Message* message)
{
	printf("Mensagem recebida:\nLength:%d\nType:%d\nSender:%s\nDestination:%s\nText:%s\n\n", 
		message->Length, message->Type, message->Sender, 
		message->Destination, message->Content);
}
